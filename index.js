// create a server with routes

let http = require("http");

const port = 3000;

const server = http.createServer((request, response) => {

	if( request.url == '/'){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end(' Server is successfully running!');
	} else if ( request.url == '/login'){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end(' Welcome to the log inpage!');
	} else {
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end(' I am sorry the page you are looking for cannot be found.');
	}
});

server.listen(port);

console.log(`Server now accessible at localhost: ${port}`);

// ctrl + c = to stop the server and rerun node 'filename.js'

